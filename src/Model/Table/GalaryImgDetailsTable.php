<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GalaryImgDetails Model
 *
 * @property \App\Model\Table\GalaryImgsTable|\Cake\ORM\Association\BelongsTo $GalaryImgs
 *
 * @method \App\Model\Entity\GalaryImgDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\GalaryImgDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GalaryImgDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImgDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GalaryImgDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GalaryImgDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImgDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImgDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class GalaryImgDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('galary_img_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('GalaryImg', [
            'foreignKey' => 'galary_img_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('font_color')
            ->maxLength('font_color', 255)
            ->allowEmpty('font_color');

        $validator
            ->scalar('fontstyle')
            ->maxLength('fontstyle', 255)
            ->allowEmpty('fontstyle');

        $validator
            ->scalar('fontsize')
            ->maxLength('fontsize', 255)
            ->allowEmpty('fontsize');

        $validator
            ->scalar('font_height')
            ->maxLength('font_height', 255)
            ->allowEmpty('font_height');

        $validator
            ->scalar('font_width')
            ->maxLength('font_width', 255)
            ->allowEmpty('font_width');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        $validator
            ->scalar('msg_title')
            ->allowEmpty('msg_title');

        $validator
            ->scalar('msg_fontstyle')
            ->maxLength('msg_fontstyle', 255)
            ->allowEmpty('msg_fontstyle');

        $validator
            ->scalar('msg_fontsize')
            ->maxLength('msg_fontsize', 255)
            ->allowEmpty('msg_fontsize');

        $validator
            ->scalar('msg_height')
            ->maxLength('msg_height', 255)
            ->allowEmpty('msg_height');

        $validator
            ->scalar('msg_width')
            ->maxLength('msg_width', 255)
            ->allowEmpty('msg_width');

        $validator
            ->scalar('msg_color')
            ->maxLength('msg_color', 255)
            ->allowEmpty('msg_color');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->integer('updated_by')
            ->allowEmpty('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->allowEmpty('status');

        $validator
            ->scalar('user_ip')
            ->maxLength('user_ip', 255)
            ->allowEmpty('user_ip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['galary_img_id'], 'GalaryImg'));

        return $rules;
    }
}
