<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GalaryImg Model
 *
 * @property |\Cake\ORM\Association\HasMany $GalaryImgDetails
 *
 * @method \App\Model\Entity\GalaryImg get($primaryKey, $options = [])
 * @method \App\Model\Entity\GalaryImg newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GalaryImg[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImg|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GalaryImg|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GalaryImg patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImg[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GalaryImg findOrCreate($search, callable $callback = null, $options = [])
 */
class GalaryImgTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('galary_img');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('GalaryImgDetails', [
            'foreignKey' => 'galary_img_id'
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'updated_at' => 'always',
                ],
            ]
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('category')
            ->allowEmpty('category');

        $validator
            ->scalar('icon_name')
            ->allowEmpty('icon_name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->integer('updated_by')
            ->requirePresence('updated_by', 'create')
            ->notEmpty('updated_by');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('user_ip')
            ->maxLength('user_ip', 255)
            ->requirePresence('user_ip', 'create')
            ->notEmpty('user_ip');

        return $validator;
    }
}
