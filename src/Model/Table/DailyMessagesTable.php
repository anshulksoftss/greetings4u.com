<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DailyMessages Model
 *
 * @property \App\Model\Table\MainCategoriesTable|\Cake\ORM\Association\BelongsTo $MainCategories
 *
 * @method \App\Model\Entity\DailyMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\DailyMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DailyMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DailyMessage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DailyMessage|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DailyMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DailyMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DailyMessage findOrCreate($search, callable $callback = null, $options = [])
 */
class DailyMessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('daily_messages');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->belongsTo('MainCategories', [
            'foreignKey' => 'main_category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->integer('updated_by')
            ->allowEmpty('updated_by');

        $validator
            ->integer('user_ip')
            ->allowEmpty('user_ip');

        $validator
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    
}
