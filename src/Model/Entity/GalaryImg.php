<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GalaryImg Entity
 *
 * @property int $id
 * @property int $category
 * @property string $description
 * @property string $name
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created_at
 * @property int $updated_by
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property int $status
 * @property string $user_ip
 */
class GalaryImg extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category' => true,
        'icon_name' => true,
        'description' => true,
        'name' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'status' => true,
        'user_ip' => true
    ];
}
