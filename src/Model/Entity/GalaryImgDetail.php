<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GalaryImgDetail Entity
 *
 * @property int $id
 * @property int $galary_img_id
 * @property string $font_color
 * @property string $fontstyle
 * @property string $fontsize
 * @property string $font_heigh
 * @property string $font_width
 * @property string $title
 * @property string $msg_title
 * @property string $msg_fontstyle
 * @property string $msg_fontsize
 * @property string $msg_height
 * @property string $msg_width
 * @property string $msg_color
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created_at
 * @property int $updated_by
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property int $status
 * @property string $user_ip
 *
 * @property \App\Model\Entity\GalaryImg $galary_img
 */
class GalaryImgDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'galary_img_id' => true,
        'font_color' => true,
        'fontstyle' => true,
        'fontsize' => true,
        'font_height' => true,
        'font_width' => true,
        'title' => true,
        'msg_title' => true,
        'msg_fontstyle' => true,
        'msg_fontsize' => true,
        'msg_height' => true,
        'msg_width' => true,
        'msg_color' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'status' => true,
        'user_ip' => true,
        'galary_img' => true
    ];
}
