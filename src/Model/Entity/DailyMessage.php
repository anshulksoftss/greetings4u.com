<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DailyMessage Entity
 *
 * @property int $id
 * @property int $main_category_id
 * @property string $name
 * @property string $description
 * @property \Cake\I18n\FrozenTime $created_at
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property int $updated_by
 * @property int $user_ip
 * @property int $status
 *
 * @property \App\Model\Entity\MainCategory $main_category
 */
class DailyMessage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'main_category_id' => true,
        'name' => true,
        'description' => true,
        'created_at' => true,
        'created_by' => true,
        'updated_at' => true,
        'updated_by' => true,
        'user_ip' => true,
        'status' => true,
        'main_category' => true
    ];
}
