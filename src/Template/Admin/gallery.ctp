<div class="content-wrapper">
  <div class="container">
      <div class="row">
          <div class="col-md-12 page-head-line">
              Gallery
              <a href="<?= url?>manage/add-image" class=" btn btn-primary" style="float: right;">
                <i class="fa fa-add"></i>&nbsp; Add More
              </a>
          </div>

      </div>
      
      <div class="row">

        <?php foreach($images as $key=>$image){?>
        <div class="col-md-4 col-sm-4">
          <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-md-4 col-sm-4">
                    #<?= ++$key ?>
                  </div>
                  <div class="col-md-3 col-sm-3 pull-right">
                    <a class="btn btn-danger" href="<?= url ?>manage/delete-image/<?= $image->id ?>">Delete</a>
                  </div>
                </div>
              </div>
              <div class="panel-body" style="padding:0px;height: 250px;overflow: scroll;">
                <a href="<?= asset.'gallery/studio?imgNm='.$image->name.'&imgId='.$image->id ?>">
                  <img width="100%" src="<?= asset.'img-gallery/'.$image->name ?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure." >
                </a>
              </div>
              <div class="panel-footer" style="height: 50px;">
                  <?= $image->description ?>
              </div>
          </div>
        </div>
        <?php } ?>
      </div>            
  </div>
</div>