<div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Manages</h1>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-6 ">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <a class="btn btn-primary" href="<?= url ?>manage/add-category">Add Categories</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Cayegory Name</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($categories as $key => $val){?>
                                        <tr>
                                            <td><?= ++$key?></td>
                                            <td><?= $val->name?></td>
                                            <td><?= $val->description?></td>
                                            <td>
                                                <!-- <a class="btn btn-primary" href="<?= url ?>manage/edit-category/<?= $val->id ?>">Edit</a> -->
                                                <a class="btn btn-danger" href="<?= url ?>manage/delete-category/<?= $val->id ?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                
                <div class="col-md-6 ">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <a class="btn btn-primary" href="<?= url ?>manage/add-message">Add Message</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($messages as $key => $val){?>
                                        <tr>
                                            <td><?= ++$key?></td>
                                            <td><?= $val->description?></td>
                                            <td>
                                                <!-- <a class="btn btn-primary" href="<?= url ?>manage/edit-category/<?= $val->id ?>">Edit</a> -->
                                                <a class="btn btn-danger" href="<?= url ?>manage/delete-category/<?= $val->id ?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
                
            </div>

        </div>
    </div>