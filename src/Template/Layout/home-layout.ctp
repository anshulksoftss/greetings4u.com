
<!DOCTYPE html>
<html>
    <head>
        <?= $this->element('head')?>
        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->css('custom.css') ?>
    </head>
    <body>
        <!--==========================
            Header
        ============================-->
          <header id="header">
            <div class="container-fluid">

              <div id="logo" class="pull-left">
                 <a href="#intro"><img src="<?= asset ?>img/logo.png" alt="" title="" style="height: 100%;" /></a>
              </div>

              <nav id="nav-menu-container">
                <ul class="nav-menu">
                  <li class="menu-active"><a href="#intro">Home</a></li><!-- 
                  <li><a href="#about">About Us</a></li>
                  <li><a href="#services">Services</a></li>
                  <li><a href="#skills">Portfolio</a></li>
                  <li><a href="career.php">Career</a></li> -->
                  <li><a href="career.php">Greetings gallery</a></li>
                  <!-- <li class="menu-has-children"><a href="">Drop Down</a>
                    <ul>
                      <li><a href="#">Drop Down 1</a></li>
                      <li><a href="#">Drop Down 3</a></li>
                      <li><a href="#">Drop Down 4</a></li>
                      <li><a href="#">Drop Down 5</a></li>
                    </ul>
                  </li> -->
                  <!-- <li><a href="#contact">Contact</a></li> -->
                </ul>
              </nav><!-- #nav-menu-container -->
            </div>
          </header><!-- #header -->
            <main id="main">
                <?php echo $this->fetch('content'); ?>
            </main>

            <?= $this->element('footer')?>
            <?= $this->element('sticky')?>
            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>
    </body>

    <?= $this->element('head-js')?>

</html>