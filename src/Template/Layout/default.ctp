<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// $cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>:
        <?= $this->fetch('title') ?>
    </title>
    <link href="<?= asset ?>img/icon.png" rel="icon">

    
  <?= $this->Html->css('style.css') ?>
  <?= $this->Html->css('custom.css') ?>

<style type="text/css">
    #main{
        min-height: 90vh;
    }
</style>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

</head>
<body>
        <!--==========================
            Header
        ============================-->
        <header id="header">
            <div class="container-fluid">

              <div id="logo" class="pull-left">
                 <a href="#about"><img src="<?= asset ?>img/logo.png" alt="" title="" style="height: 100%;" /></a>
              </div>

                
              <nav id="nav-menu-container">
                <ul class="nav-menu">
                  <!-- <li class="menu-active"><a href="#about">Home</a></li>
                  <li><a href="#about">Login</a></li> -->
                  <!-- <li><a href="#services">Services</a></li>
                  <li><a href="#skills">Portfolio</a></li>
                  <li><a href="career.php">Career</a></li> -->
                  <!-- <li class="menu-has-children"><a href="">Drop Down</a>
                    <ul>
                      <li><a href="#">Drop Down 1</a></li>
                      <li><a href="#">Drop Down 3</a></li>
                      <li><a href="#">Drop Down 4</a></li>
                      <li><a href="#">Drop Down 5</a></li>
                    </ul>
                  </li> -->
                  <!-- <li><a href="#contact">Contact</a></li> -->
                </ul>
              </nav> <!-- #nav-menu-container -->
            </div>
          </header><!-- #header -->
        <main id="main">
                <?= $this->fetch('content') ?>
        </main>

        <?= $this->element('footer')?>
        <!-- <?= $this->element('sticky')?> -->
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


</body>


</html>
