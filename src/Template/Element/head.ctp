
  <title><?= $this->fetch('title')?></title>
<!-- Favicons -->
  <link href="<?= asset ?>img/icon.png" rel="icon">
  <link href="<?= asset ?>img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?= asset ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?= asset ?>lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= asset ?>lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?= asset ?>lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?= asset ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= asset ?>lib/lightbox/css/lightbox.min.css" rel="stylesheet">
  
  <script src="<?= asset ?>lib/jquery/jquery.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    var auth=<?= json_encode($auth) ?>;
    console.log(auth);
  </script>