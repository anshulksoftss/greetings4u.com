<footer id="footer">
		<div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Ksoft Smart Solutions</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
        Phone:<strong> +91 11 45625653 </strong>|
        Email:<strong> info@ksoftss.com </strong>
      </div>
    </div>
</footer><!-- #footer -->