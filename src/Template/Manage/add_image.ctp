
<style type="text/css">
    .custom-file-container{
        box-sizing:border-box;position:relative;display:block
    }

    .custom-file-container__custom-file{
        box-sizing:border-box;
        position:relative;
        display:inline-block;
        width:100%;
        height:calc(2.25rem + 2px);
        margin-bottom:0;
        margin-top:5px
    }
    
    .custom-file-container__custom-file:hover{cursor:pointer}
    
    .custom-file-container__custom-file__custom-file-input{
        box-sizing:border-box;
        min-width:14rem;
        max-width:100%;
        height:calc(2.25rem + 2px);
        margin:0;
        opacity:0
    }
    
    .custom-file-container__custom-file__custom-file-control{box-sizing:border-box;position:absolute;top:0;right:0;left:0;z-index:1;height:calc(2.25rem + 5px);padding:.5rem .75rem;overflow:hidden;line-height:1.5;color:#333;user-select:none;background-color:#fff;background-clip:padding-box;border:1px solid #c0c0af;border-radius:.25rem}
    
    .custom-file-container__custom-file__custom-file-control:empty:after{box-sizing:border-box;content:"Choose file..."}

    .custom-file-container__custom-file__custom-file-control:before{position:absolute;top:0;right:0;z-index:2;display:block;height:calc(2.25rem + 2px);padding:.5rem .75rem;line-height:1.25;color:#333;background-color:#edede8;border-left:1px solid #c0c0af;box-sizing:border-box;content:"Browse"}

    .custom-file-container__image-preview{
        transition:all .2s ease;
        margin-top:20px;
        margin-bottom:40px;
        height:400px;width:100%;
        border-radius:4px;
        border:2px solid #b3b3b3;
        background-color:#fff;
        overflow:hidden;
    }

    .custom-file-container__image-multi-preview,.custom-file-container__image-preview{box-sizing:border-box;background-size:cover;background-position:50%;background-repeat:no-repeat}

    .custom-file-container__image-multi-preview{
        transition:all .6s ease;
        border-radius:6px;
        float:left;margin:1.858736059%;
        width:29.615861214%;height:90px;
        box-shadow:0 4px 10px 0 rgba(51,51,51,.25);
    }

    #myUniqueThumbUploadId{
        width: 25%;
    }

    #myUniqueThumbUploadId .custom-file-container__image-preview{
        height: 100px;
    }
</style>

<?php 
$icons = (object)array
(
    "anniversary",
    "birthday",
    "friendship",
    "valentine",
    "father",
    "mother",
    "women",
    "teacher",
    "republic",
    "thankyou",
    "sorry",
    "hi",
    "babyshower",
    "graduation",
    "loveyou",
    "missyou",
    "getwell",
);
?>

<div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Manages</h1>
                </div>
            </div>
            <div class="row col-md-offset-2">
                <div class="col-md-10">
                    <?= $this->Flash->render()?>
                </div>
            </div>
            
            <div class="row col-md-offset-2">
                <div class="col-md-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Add New Image
                        </div>
                        <div class="panel-body">
                        <?= $this->Form->create($data,['type'=>'file']) ?>
                            <div class="custom-file-container" data-upload-id="myUniqueUploadId">
                                <label>Upload Image <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                                <label class="custom-file-container__custom-file" >
                                    <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="*" name="uploaded_image" required>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                </label>
                                <div class="custom-file-container__image-preview"></div>
                            </div>
                            <div class="custom-file-container" data-upload-id="myUniqueThumbUploadId" id="myUniqueThumbUploadId">
                                <label>Upload Thumbnail Image <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                                <label class="custom-file-container__custom-file" id="thumbUploadId">
                                    <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="*" name="upload_thumb_image" required>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                </label>
                                <div class="custom-file-container__image-preview"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" >Category</label>
                                        <select class="form-control" name="category" required>
                                            <option value="">Select One</option>
                                            <?php foreach ($category as $key => $value) {
                                                echo "<option value='".$value->id."'>".$value->name."</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" >Icon name</label>
                                        <select class="form-control" name="icon_name">
                                            <option value="">Select One (optional)</option>
                                            <?php foreach ($icons as $key => $value) {
                                                echo "<option value='".$value."'>".$value."</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->input('name',['class'=>'form-control','required'=>true,'placeholder'=>'Name', 'required']);?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->textarea('description',['class'=>'form-control','required'=>true,'placeholder'=>'Description']);?>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pull-right">
                                    <button type="submit" class="btn btn-success">Proceed to Add Details <b>&#8594;</b></button>
                                </div>
                            </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://unpkg.com/file-upload-with-preview"></script> 
<script>
    var upload = new FileUploadWithPreview('myUniqueUploadId')
    var upload = new FileUploadWithPreview('myUniqueThumbUploadId')
</script> 