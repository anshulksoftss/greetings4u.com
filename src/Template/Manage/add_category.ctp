<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Manages</h1>
            </div>
        </div>
        <div class="row col-md-offset-2">
            <div class="col-md-8">
                <?= $this->Flash->render()?>
            </div>
        </div>
        
        <div class="row col-md-offset-2">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       Add New Category
                    </div>
                    <div class="panel-body">
                    <?= $this->Form->create() ?>
                        <div class="form-group">
                            <?= $this->Form->input('name', ['class' => 'form-control','required'=>true,'placeholder'=>'Category Name']); ?>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description</label>
                            <?php echo $this->Form->textarea('description', ['class' => 'form-control','required'=>true,'placeholder'=>'Category Description']); ?>
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
