
    
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">

<script src="<?=asset?>lib/jquery/jquery.min.js"></script>

<style type="text/css">
  label{
    position: inherit;
  }

  select.form-control:not([size]):not([multiple]) {
    height: calc(3.25rem + 2px);
  }

</style>

<style>

@font-face {
    font-family: myFirstFont;
    src: url(<?=asset?>webroot/font/Fonts/font.ttf);
}

@font-face {
    font-family: bollivia-rosilla;
    src: url(<?=asset?>webroot/font/Fonts/bollivia-rosilla.ttf);
}

@font-face {
    font-family: fabulous;
    src: url(<?=asset?>webroot/font/Fonts/fabulous.ttf);
}

@font-face {
    font-family: Humble;
    src: url(<?=asset?>webroot/font/Fonts/Humble.ttf);
}

@font-face {
    font-family: News701BTItalic;
    src: url(<?=asset?>webroot/font/Fonts/News701BTItalic.ttf);
}

@font-face {
    font-family: OratorStd;
    src: url(<?=asset?>webroot/font/Fonts/OratorStd.otf);
}

@font-face {
    font-family: palab;
    src: url(<?=asset?>webroot/font/Fonts/palab.ttf);
}

@font-face {
    font-family: Radeshia;
    src: url(<?=asset?>webroot/font/Fonts/Radeshia.ttf);
}

@font-face {
    font-family: RolleteQaku-Regular;
    src: url(<?=asset?>webroot/font/Fonts/RolleteQaku-Regular.ttf);
}

.figure {
    position: relative;
    text-align: center;
    color: white;
}

.figure-caption{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-family: myFirstFont;
}

#msg{
  top:60%;
  left: 50%;
}

.bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
}

.top-left {
    position: absolute;
    top: 8px;
    left: 16px;
}

.top-right {
    position: absolute;
    top: 8px;
    right: 16px;
}

.bottom-right {
    position: absolute;
    bottom: 8px;
    right: 16px;
}

.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}

.top {
    position: absolute;
    top: 10%;
    left: 50%;
    transform: translate(-50%, -50%);
}

.bottom {
    position: absolute;
    top: 90%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>

<div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">Studio</h1>
                </div>
            </div>
            <div class="row col-md-offset-2">
                <div class="col-md-10">
                    <?= $this->Flash->render()?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <?= $title?>
                        </div>
                        <div class="panel-body">
                          <?= $this->Form->create($data) ?>
                          <div class="col-md-3">
                            <div class="row">
                              <!-- <div class="col-md-12"> -->
                                <div class="col-md-12">
                                  <label for="title">Text/Title</label><br>
                                  <input type="text" name="title" id="title" class="form-control" required>
                                </div>
                                <div class="col-md-12">
                                  <label for="fontstyle">Font Style</label><br>
                                  <select name="fontstyle" id="fontstyle" class="form-control">
                                  <!-- <option value="0">0</option> -->
                                  <?php 
                                    $styles = array('font', 'bollivia-rosilla', 'Fabulous', 'Humble', 'News701BTItalic', 'Palab', 'Radeshia', 'RolleteQaku-Regular');
                                    foreach ($styles as $key) {
                                      echo '<option value="'.$key.'">'.$key.'</option>';
                                    }
                                  ?>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                <label for="fontsize">Font Size</label>
                                <select name="fontsize" id="fontsize" class="form-control">
                                <!-- <option value="0">0</option> -->
                                <?php 
                                  $number = range(10,100,5);
                                  foreach ($number as $key => $value) {
                                    echo '<option value="'.$value.'">'.$value.'</option>';
                                  }
                                ?>
                                </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="font_color">Color</label>
                                    <input type="color" style="padding:0px 2px" name="font_color" id="font_color" class="form-control" required>
                                </div>
                                <div class="col-md-12">
                                  <h4 for="font_color"><b>Position</b></h4>
                                </div>
                                <div class="col-md-6">
                                    <label for="height">Horizontal</label><br>
                                    <input type="number" name="font_height" id="font_height" class="form-control" min="0" value="50" max="100" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="width">Vertical</label><br>
                                    <input type="number" name="font_width" id="font_width" class="form-control" min="0" value="50" max="100" required>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <h3><b>Your Custom Message</b></h3>
                                <label for="msg_title">Message</label><br>
                                <textarea name="msgtitle" id="msgtitle" class="form-control" rows="5" placeholder="Please input a custom message or leave empty" >Your Custom Message</textarea>
                                <!-- <input type="number" name="fontsize" id="fontsize" class="form-control" max="100" required><br> -->
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <label for="msg_fontstyle">Style</label><br>
                                <select name="msg_fontstyle" id="msg_fontstyle" class="form-control">
                                <!-- <option value="0">0</option> -->
                                <?php 
                                  $styles = array('font', 'bollivia-rosilla', 'Fabulous', 'Humble', 'News701BTItalic', 'Palab', 'Radeshia', 'RolleteQaku-Regular');
                                  foreach ($styles as $key) {
                                    echo '<option value="'.$key.'">'.$key.'</option>';
                                  }
                                ?>
                                </select>
                              </div>
                              <div class="col-md-6">
                                <label for="msg_fontsize">Size</label><br>
                                <select name="msg_fontsize" id="msg_fontsize" class="form-control">
                                <!-- <option value="0">0</option> -->
                                <?php 
                                  $number = range(10,50,5);
                                  foreach ($number as $key => $value) {
                                    echo '<option value="'.$value.'">'.$value.'</option>';
                                  }
                                ?>
                                </select>
                              </div>
                              <div class="col-md-6">
                                <label for="msg_height">Horizontal</label><br>
                                <input type="number" name="msg_height" id="msg_height" class="form-control" min="0" value="60" max="100" required><br>
                              </div>
                              <div class="col-md-6">
                                <label for="msg_width">Vertical</label><br>
                                <input type="number" name="msg_width" id="msg_width" class="form-control" min="0" value="50" max="100" required><br>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                          <figure class="figure">
                            <img src="<?= asset ?>img-gallery/<?= $img_data->name ?>" class="figure-img img-fluid rounded" width="100%" alt="A generic square placeholder image with rounded corners in a figure.">
                            <figcaption class="figure-caption rounded" id="img1" style="color:black;font-size:30px" >Your name</figcaption>
                            <figcaption class="figure-caption rounded" id="msg" style="color:black;font-size:30px" >Your custom message</figcaption>
                          </figure>
                        </div>
                        <div class="col-md-12">
                          <div class="row">
                              <div class="col-md-12">
                                <br><button type="submit" class="btn btn-lg btn-block btn-success">Save Detais</button>
                              </div>
                          </div>
                        </div>
                      <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.js"></script>
<script>
    $(function() {
        $('#font_color').colorpicker({
            color: "#88cc33",
            horizontal: true
        }).on('changeColor',
            function(ev) {
                changeFont();
                changeMsg();
            });
    });
</script>

<script type="text/javascript">
// $('#color').colorpicker();

$(' #title, #font_height, #font_width ').on('keyup',function () {
  changeFont();
});

$('#fontsize, #font_color, #fontstyle').on('change',function () {
  changeFont();
});

function changeFont(){
  var title = $('#title').val();
  var height = $('#font_height').val();
  var width = $('#font_width').val();
  var fontsize = $('#fontsize option:selected').val();
  var fontstyle = $('#fontstyle option:selected').val();
  var fontColor = $('#font_color').val();
  
  // title = title.toUpperCase();

  $('#img1').text(title).css({"top":height+"%", "left":width+"%", "color":fontColor,"font-size":fontsize+"px","font-family":fontstyle});
}

// Custom message
$(' #msgtitle, #msg_height, #msg_width ').on('keyup',function () {
  changeMsg();
});

$('#msg_fontsize, #font_color, #msg_fontstyle').on('change',function () {
  changeMsg();
});

function changeMsg(){
  var title = $('#msgtitle').val();
  var height = $('#msg_height').val();
  var width = $('#msg_width').val();
  var fontsize = $('#msg_fontsize option:selected').val();
  var fontstyle = $('#msg_fontstyle option:selected').val();
  var fontColor = $('#font_color').val();

  // title = title.toUpperCase();

  $('#msg').text(title).css({"top":height+"%", "left":width+"%", "color":fontColor,"font-size":fontsize+"px","font-family":fontstyle}).text(title);
}

</script>