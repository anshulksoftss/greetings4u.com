<style type="text/css">
  label{
    position: inherit;
  }

  select.form-control:not([size]):not([multiple]) {
    height: calc(3.25rem + 2px);
  }

  /*.figure-caption {
    position: absolute;    bottom: 20px;    right: 20px;    background-color: #00000042;    color: white;    padding-left: 20px;    padding-right: 20px;
  }*/

</style>

<style>

@font-face {
    font-family: myFirstFont;
    src: url(<?=asset?>webroot/font/Fonts/font.ttf);
}

@font-face {
    font-family: bollivia-rosilla;
    src: url(<?=asset?>webroot/font/Fonts/bollivia-rosilla.ttf);
}

@font-face {
    font-family: fabulous;
    src: url(<?=asset?>webroot/font/Fonts/fabulous.ttf);
}

@font-face {
    font-family: Humble;
    src: url(<?=asset?>webroot/font/Fonts/Humble.ttf);
}

@font-face {
    font-family: News701BTItalic;
    src: url(<?=asset?>webroot/font/Fonts/News701BTItalic.ttf);
}

@font-face {
    font-family: OratorStd;
    src: url(<?=asset?>webroot/font/Fonts/OratorStd.otf);
}

@font-face {
    font-family: palab;
    src: url(<?=asset?>webroot/font/Fonts/palab.ttf);
}

@font-face {
    font-family: Radeshia;
    src: url(<?=asset?>webroot/font/Fonts/Radeshia.ttf);
}

@font-face {
    font-family: RolleteQaku-Regular;
    src: url(<?=asset?>webroot/font/Fonts/RolleteQaku-Regular.ttf);
}

.figure {
    position: relative;
    text-align: center;
    color: white;
}

.figure-caption{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-family: myFirstFont;
}

.bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
}

.top-left {
    position: absolute;
    top: 8px;
    left: 16px;
}

.top-right {
    position: absolute;
    top: 8px;
    right: 16px;
}

.bottom-right {
    position: absolute;
    bottom: 8px;
    right: 16px;
}

.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}

.top {
    position: absolute;
    top: 10%;
    left: 50%;
    transform: translate(-50%, -50%);
}

.bottom {
    position: absolute;
    top: 90%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>
    
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">

<section id="about" style="height:28vh">
  <div class="container">

    <header class="section-header" style="padding-top:50px">
      <h3>Studio</h3>
    </header>
    <div class="row">

    </div>
  </div>
</section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <!-- <h3>Services</h3> -->
          <!-- <p>ERP is business process management software that allows an organization to use a system of integrated applications.</p> -->
        </header>
        <?php 
          $galary_img_details = (object)array
        (
            "galary_img_id" => null,
            "font_color" => null,
            "fontstyle" => null,
            "fontsize" => null,
            "font_height" => null,
            "font_width" => null,
            "title" => null,
            "msg_title" => null,
            "msg_fontstyle" => null,
            "msg_fontsize" => null,
            "msg_height" => null,
            "msg_width" => null,
            "msg_color" => null,
            "created_by" => null,
            "created_at" => null,
            "updated_by" => null,
            "updated_at" => null,
            "status" => null,
            "user_ip" => null,
            "galary_img" => null,
        );

          $galary_img_details = $data->galary_img_details?$data->galary_img_details[0]:$galary_img_details; 
           // = $data->galary_img_details[0]; 

        ?>
        <div class="row">
          <div class="col-md-12 col-sm-12">
              <figure class="figure">
                <img src="<?= asset ?>img-gallery/<?= $img ?>" class="figure-img img-fluid rounded" width="1000px" alt="A generic square placeholder image with rounded corners in a figure.">
                <figcaption class="figure-caption rounded" id="img1" style="color:black;font-size:30px" ></figcaption>
                <figcaption class="figure-caption rounded" id="msg" style="color:black;font-size:30px" ></figcaption>
              </figure>
          </div>
        </div>
        
        <?= $this->Form->create(null, ['action'=>'generateImage']) ?>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <!-- <h2>Styling</h2> -->
            <div class="col-md-12">
                <h3><b>Title</b></h3>
              <label for="title">Text/Title</label><br>
              <input type="text" name="title" value="<?= $galary_img_details->title ?>" ="title" id="title" class="form-control" required>
            </div>
            <div class="col-md-12">
              <label for="fontstyle">Font Style</label><br>
              <select name="fontstyle" id="fontstyle" class="form-control">
              <!-- <option value="0">0</option> -->
              <?php 
                $styles = array('Font', 'Bollivia-rosilla', 'Fabulous', 'Humble', 'News701BTItalic', 'Palab', 'Radeshia', 'RolleteQaku-Regular');
                foreach ($styles as $key) {
                  if($value == $galary_img_details->fontstyle){
                    echo '<option value="'.$key.'" selected>'.$key.'</option>';
                  }
                  else{
                    echo '<option value="'.$key.'">'.$key.'</option>';
                  }
                }
              ?>
              </select>
            </div>
            <div class="col-md-6">
            <label for="fontsize">Font Size</label>
            <select name="fontsize" id="fontsize" class="form-control">
            <!-- <option value="0">0</option> -->
            <?php 
                $number = range(10,100,5);

                foreach ($number as $key => $value) {
                  if($value == $galary_img_details->fontsize){
                    echo '<option value="'.$value.'" selected>'.$value.'</option>';
                  }
                  else{
                    echo '<option value="'.$value.'">'.$value.'</option>';
                  }
                  
                }
              ?>
            </select>
            </div>
            <div class="col-md-6">
                <label for="font_color">Color</label>
                <input type="textarea" name="font_color" value="<?= $galary_img_details->font_color ?>" id="font_color" class="form-control" required>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <label for="height">Horizontal</label>
                <input type="number" name="font_height" id="font_height" value="<?= $galary_img_details->font_height ?>" class="form-control" min="0" value="50" max="100" required>
            </div>
            <div class="col-md-6">
                <label for="width">Vertical</label><br>
                <input type="number" name="font_width" id="font_width" value="<?= $galary_img_details->font_width ?>"  class="form-control" min="0" value="50" max="100" required>
            </div>
          
          </div>

          <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <h3><b>Message</b></h3>
                    <label for="msg_title">Message</label><br>
                    <!-- <textarea name="msgtitle" id="msgtitle" class="form-control dropdown" rows="4" placeholder="Please input a custom message or leave empty" ><?= $galary_img_details->msg_title ?></textarea> -->
                    <div class="dropdown">
                      <textarea class="form-control dropdown-toggle" data-toggle="dropdown" name="msgtitle" id="msgtitle" rows="4" placeholder="Please input a custom message or leave empty"></textarea>
                      <ul class="dropdown-menu" style="width: 100%;">
                      </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="msg_fontstyle">Style</label><br>
                    <select name="msg_fontstyle" id="<?= $galary_img_details->msg_fontstyle ?>" class="form-control">
                    <!-- <option value="0">0</option> -->
                    <?php 
                      $styles = array('Font', 'Bollivia-rosilla', 'Fabulous', 'Humble', 'News701BTItalic', 'Palab', 'Radeshia', 'RolleteQaku-Regular');
                      foreach ($styles as $key) {
                        echo '<option value="'.$key.'">'.$key.'</option>';
                      }
                    ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="msg_fontsize">Size</label><br>
                    <select name="msg_fontsize" id="msg_fontsize" class="form-control">
                    <!-- <option value="0">0</option> -->
                    <?php 
                      $number = range(10,50,5);
                      foreach ($number as $key => $value) {
                        echo '<option value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="msg_height">Horizontal</label><br>
                    <input type="number" name="msg_height" id="msg_height" class="form-control" min="0" value="<?= $galary_img_details->msg_height ?>" max="100" ><br>
                </div>
                <div class="col-md-6">
                    <label for="msg_width">Vertical</label><br>
                    <input type="number" name="msg_width" id="msg_width" class="form-control" min="0" value="<?= $galary_img_details->msg_width ?>" max="100" ><br>
                </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <button type="button" class="btn btn-info btn-block btn-lg downloadbtn" >Download</button> <!-- data-toggle="modal" data-target="#exampleModalCenter" -->
            <button type="submit" class="btn btn-lg btn-block btn-success" id="img_download">Download Image</button>
          </div>
          <div class="col-md-6">
            <button type="reset" class="btn btn-lg btn-block btn-danger" >Reset</button>
          </div>
        </div>

      <?= $this->Form->end() ?>

      </div>
    </section><!-- #services -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Register with us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, ['url' => ['controller' => 'users', 'action' => 'add']]) ?>
      <div class="modal-body">
      <div class="form-group">
        <label for="nameInputEmail1">Name</label>
        <input type="text" class="form-control" id="nameInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" name="name">
        <small id="nameHelp" class="form-text text-muted"></small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
        <small id="emailHelp" class="form-text text-muted"></small>
      </div>
      <div class="form-group">
        <label for="mobileInput">Mobile</label>
        <input type="number" class="form-control" id="mobileInput" placeholder="Mobile Number" name="mobile">
      </div>
      <div class="form-group">
        <label for="mobileInput">Password</label>
        <input type="password" class="form-control" id="password" placeholder="password" name="password">
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="add_user">Register and Download</button>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.js"></script>

<script type="text/javascript">

window.onload = changeFont();
window.onload = changeMsg();

$(document).ready(function () {
  $('#img_download').hide();  
})


$(' #title, #font_height, #font_width ').on('keyup',function () {
  changeFont();
});

$('#fontsize, #font_color, #fontstyle').on('change',function () {
  changeFont();
});

function changeFont(){
  var title = $('#title').val();
  var height = $('#font_height').val();
  var width = $('#font_width').val();
  var fontsize = $('#fontsize option:selected').val();
  var fontstyle = $('#fontstyle option:selected').val();
  var fontColor = $('#font_color').val();
  
  $('#img1').text(title).css({"top":height+"%", "left":width+"%", "color":fontColor,"font-size":fontsize+"px","font-family":fontstyle});
}

// Custom message
$(' #msgtitle, #msg_height, #msg_width ').on('keyup',function () {
  changeMsg();
});

$('#msg_fontsize, #font_color, #msg_fontstyle').on('change',function () {
  changeMsg();
});

function changeMsg(){
  var title = $('#msgtitle').val();
  var height = $('#msg_height').val();
  var width = $('#msg_width').val();
  var fontsize = $('#msg_fontsize option:selected').val();
  var fontstyle = $('#msg_fontstyle option:selected').val();
  var fontColor = $('#font_color').val();

  $('#msg').text(title).css({"top":height+"%", "left":width+"%", "color":fontColor,"font-size":fontsize+"px","font-family":fontstyle}).text(title);
}

</script>

<script>
    $(function() {
        $('#font_color').colorpicker({
            color: "#88cc33",
            horizontal: true
        }).on('changeColor',
            function(ev) {
                changeFont();
                changeMsg();
            });
    });
</script>

<script type="text/javascript">
  $(function(){
    $(".downloadbtn").on("click", function(event) {
        event.preventDefault();
        $.ajax({
          url: "<?= url ?>gallery/chk-user",
          type: "GET",
          // data: {data : JSON.stringify(formData)},
          success: function(result) {
            resp=JSON.parse(result);
            console.log(resp.status);
            if (resp.status == 'success') { 
              $( "#img_download" ).trigger( "click" );
            }
            else{ console.log('true');
              $( "#exampleModalCenter" ).modal('toggle');
            }
          }
      });
    });
  });

  $(function(){
    $("#add_user").on("click", function(event) {
      event.preventDefault();

      var formData = {
          'name': $('#nameInputEmail1').val(), //for get email 
          'username': $('#exampleInputEmail1').val(), 
          'mobile': $('#mobileInput').val(), 
          'password': $('#password').val(), 
          'role': '2'
      };

      $.ajax({
          url: "<?= url ?>gallery/add-user",
          // url: "http://localhost/greetings4u/gallery/add-user",
          type: "GET",
          data: {data : JSON.stringify(formData)},
          success: function(result) {
            console.log(result);
            if (result == "success") {
              $( ".close" ).trigger( "click" );
              alert('Thanks for register with us!');
              $( "#img_download" ).trigger( "click" );
            }
            else{
              $( ".close" ).trigger( "click" );
              alert('You are already registered!');
              $( "#img_download" ).trigger( "click" );
            }
          }
      });
    });

    // $( "#msgtitle" ).autocomplete({
    //   source:'<?= asset ?>gallery/';
    // });

    $("#msgtitle").on("keyup", function() { 
        var value = $(this).val().toLowerCase();
        // $(".dropdown-menu li").filter(function() {
        //     $(this).toggle($(this).val().toLowerCase().indexOf(value) > -1)
        // });
        var url="<?= asset ?>gallery/messages-by-id/"+$(this).val(); 
        $('.dropdown-menu').load(url);
    });



  })

  function copyToMessage(element) {
    console.log(element);
    $('#msgtitle').val(element);
  }
</script>