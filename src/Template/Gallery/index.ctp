
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?= asset ?>css/gallery-grid.css">
    <link href="<?= asset ?>lib/textillate/animate.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
    <style type="text/css">
        #message {
            background: #fff;
            background-size: cover;
            /*position: absolute;*/
            left: 0px;
            padding: 60px 0 40px 0;
        }

        #category {
            background: #fff;
            background-size: cover;
            /*position: absolute;*/
            right: 0px;
            padding: 60px 0 40px 0;
        }

        .message-container {
            background-color: #ececec;
            border-radius: 10px;
            box-shadow: 0 8px 15px rgba(0, 0, 0, 0.06);
            padding: 1px 20px;
            min-height: 50vh;
            font-size: 15px;
        }

        .category-container {
            background-color: #ececec;
            border-radius: 10px;
            box-shadow: 0 8px 15px rgba(0, 0, 0, 0.06);
            padding: 1px 20px;
            min-height: 50vh;
            font-size: 15px;
        }

        .gallery-container{
            width: 85%;
        }

        .hr-line{
            margin-top: 8px;
            margin-bottom: 10px;
            border-top: 1px solid #b3b3b3;
        }

        .tz-gallery {
            display: block;
            max-width: 100%;
            
        }

        .tz-gallery .occations img{
            height: 90px;
            width: 90px;
            transition: 0.2s ease-in-out;
            box-shadow: none;
            margin: 10px;
            /*border: 4px dotted #c1c1d0;border-radius: 10px;*/
        }

        .tz-gallery .occations img:hover {
            transform: scale(1.2);
            box-shadow:none;
        }

        .tz-gallery .occations table.table tr td a {
            text-decoration: none;
            display: block;
            float: left;
        }

        .tz-gallery .occations a:hover {
            text-decoration: none;
        }

        .table {
            margin-bottom:0px; 
        }

        .table>tbody>tr>td {
            padding-bottom: 0px;
        }
    </style>

    <style>
    @-webkit-keyframes swinging{
        0%{-webkit-transform: rotate(10deg);}
        50%{-webkit-transform: rotate(-5deg)}
        100%{-webkit-transform: rotate(10deg);}
    }

    @keyframes swinging{
        0%{transform: rotate(10deg);}
        50%{transform: rotate(-5deg)}
        100%{transform: rotate(10deg);}
    }

    .swingimage{
        position: relative;
        left: 50px;
        -webkit-transform-origin: 50% 0;
        transform-origin: 50% 0;
        -webkit-animation: swinging 3.5s ease-in-out forwards infinite;
        animation: swinging 3.5s ease-in-out forwards infinite;
    }

    .swingimageshift{
        position: relative;
        left: 50px;
        margin-top: 30px;
        -webkit-transform-origin: 35% 0;
        transform-origin: 35% 0;
        -webkit-animation: swinging 3.5s ease-in-out forwards infinite;
        animation: swinging 3.5s ease-in-out forwards infinite;
    }

    .figure {
        position: relative;
        text-align: center;
        color: white;
    }

    .figure-caption{
      position: absolute;
      top: 60%;
      left: 50%;
      transform: translate(-50%, -50%);
        font-family: 'Patrick Hand', cursive;
        font-size: 45px;
    }
    </style>

<section id="about" style="height: 28vh">
  <div class="container">

    <header class="section-header" style="padding-top:25px">
      <h3 class="glow in tlt header" id="greetings" style="font: 400 50px/0.8 'Great Vibes', Helvetica, sans-serif;  color: #1909dc; text-shadow: 4px 4px 3px rgb(193, 24, 24); font-weight: bold;">Greetings</h3>
      <p>Make Your own greetings with us.</p>
    </header>
    <div class="row">

    </div>
  </div>
</section><!-- #about -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <section id="services">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <figure class="figure">
                            <img src="<?= asset ?>img/title-scroll.png" class="figure-img img-fluid rounded" width="40%" height="115px" alt="A generic square placeholder image with rounded corners in a figure.">
                            <figcaption class="figure-caption rounded" id="img1" >OCCASION</figcaption>
                          </figure>
                    </div>
                </div>
            <div class="">
                <!-- <h2 style="text-align: center;">OCCASIONS</h2> -->
                <div class="tz-gallery" style="text-align: center;"> 
                    <div style="display: block;border: 4px dotted #c1c1d0;border-radius: 10px;">
                        <!-- <div style="position: absolute;left: 2px;top: 65px;">
                            <img class="swingimage" border="0" src="<?= asset.'img/tags/occation.png' ?>" alt="http://www.javascriptkit.com/dhtmltutors/wintersale.png" height="225px">
                        </div> title-scroll.png-->
                        
                        <div class="occations table-responsive" >
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/anniversary'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/anniversary.png' ?>">
                                                <h4>Anniversary</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/birthday'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/birthday.png' ?>">
                                                <h4>Birthday</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/friendship'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/friendship.png' ?>">
                                                <h4>Friendship day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/valentine'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/valentine.png' ?>">
                                                <h4>Valentine's day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/father'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/father.png' ?>">
                                                <h4>Father's day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/mother'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/mother.png' ?>">
                                                <h4>Mother's day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/women'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/women.png' ?>">
                                                <h4>Women's day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/teacher'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/teacher.png' ?>">
                                                <h4>Teacher's day</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/republic'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/republic.png' ?>">
                                                <h4>Independence day <br>Republic's day</h4>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
        <div class="col-lg-12">
            <section id="services" style="padding-top: 0px;">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <figure class="figure">
                            <img src="<?= asset ?>img/title-scroll.png" class="figure-img img-fluid rounded" width="40%" height="115px" alt="A generic square placeholder image with rounded corners in a figure.">
                            <figcaption class="figure-caption rounded" id="img1" >ALWAYS THERE</figcaption>
                          </figure>
                    </div>
                </div>
            <div class="">
                <!-- <h2 style="text-align: center;">ALWAYS THERE</h2> -->
                <div class="tz-gallery" style="text-align: center;"> 
                    <div style="display: block;border: 4px dotted #c1c1d0;border-radius: 10px;">
                        <!-- <div style="position: absolute;right: 90px;top:10px">
                            <img class="swingimage" border="0" src="<?= asset.'img/tags/always.png' ?>" alt="http://www.javascriptkit.com/dhtmltutors/wintersale.png" height="220px">
                        </div> -->
                        <div class="occations table-responsive">
                            <table class="table table-flex">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/thankyou'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/thankyou.png' ?>">
                                                <h4>Thank you</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/sorry'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/sorry.png' ?>">
                                                <h4>Sorry</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/hi'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/hi.png' ?>">
                                                <h4>Hi</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/babyshower'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/babyshower.png' ?>">
                                                <h4>Baby shower</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/graduation'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/graduation.png' ?>">
                                                <h4>Graduation</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/loveyou'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/loveyou.png' ?>">
                                                <h4>Love you</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/missyou'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/missyou.png' ?>">
                                                <h4>Miss You</h4>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= asset.'gallery/festival/getwell'?>">
                                                <img class="" src="<?= asset.'img/icons/icon/getwell.png' ?>">
                                                <h4>Get well</h4>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <section id="services">
            <div class="">
                <?php foreach($data as $key=>$val){ ?>
                <a class="lightbox" href="gallery/category-id/<?= $val->id ?>">
                    <h2 style="text-align: center;"><?= $val->name ?></h2>
                </a>
                <div class="tz-gallery" style="text-align: center;"> 
                    <div style="overflow: overlay;display: inline-flex;border: 4px dotted #c1c1d0;border-radius: 10px;"> <hr>
                        <?php foreach($val->galery_img as $key2=>$image){?>
                            <div class="lightbox" style="padding:20px">
                                <!-- <a href="<?= asset.'gallery/studio?imgNm='.$image->name.'&imgId='.$image->id ?>"> -->
                                <a href="<?= asset.'img-gallery/'.$image->name?>">
                                    <img src="<?= asset.'img-gallery/thumbs/'.$image->name ?>" style="height: 150px;width: 200px" 
                                    alt="<?= asset.'gallery/studio?imgNm='.$image->name.'&imgId='.$image->id ?>" onerror="this.src = '<?= asset ?>img/no_photo_available.jpg'">
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <hr>  
                <?php } ?>
            </div>
            </section>
        </div>
    </div>
</div>

<script src="<?= asset ?>lib/textillate/jquery.fittext.js"></script>
<script src="<?= asset ?>lib/textillate/jquery.lettering.js"></script>
<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<script src="<?= asset ?>js/jquery.textillate.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<script>
    // $('#greetings').textillate({ in: { effect: 'fadeInLeftBig' } });

$('#greetings').textillate({
  // the default selector to use when detecting multiple texts to animate
  selector: '.texts',

  // enable looping
  loop: true,

  // sets the minimum display time for each text before it is replaced
  minDisplayTime: 2000,

  // sets the initial delay before starting the animation
  // (note that depending on the in effect you may need to manually apply
  // visibility: hidden to the element before running this plugin)
  initialDelay: 0,

  // set whether or not to automatically start animating
  autoStart: true,

  // custom set of 'in' effects. This effects whether or not the
  // character is shown/hidden before or after an animation
  inEffects: [],

  // custom set of 'out' effects
  outEffects: [ 'hinge' ],

  // in animation settings
  in: {
    // set the effect name
    effect: 'flip',

    // set the delay factor applied to each consecutive character
    delayScale: 2,

    // set the delay between each character
    delay: 100,

    // set to true to animate all the characters at the same time
    sync: false,

    // randomize the character sequence
    // (note that shuffle doesn't make sense with sync = true)
    shuffle: true,

    // reverse the character sequence
    // (note that reverse doesn't make sense with sync = true)
    reverse: false,

    // callback that executes once the animation has finished
    callback: function () {}
  },

  // out animation settings.
  out: {
    effect: 'swing',
    delayScale: 1.5,
    delay: 50,
    sync: false,
    shuffle: true,
    reverse: false,
    callback: function () {}
  },

  // callback that executes once textillate has finished
  callback: function () {},

  // set the type of token to animate (available types: 'char' and 'word')
  type: 'char'
});
</script>

<script>

    // $('.header').addClass('animated bounce');
    $('.section-header p').addClass('animated bounceInDown');

  
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>

<script>
    baguetteBox.run('.tz-gallery', {
        captions: function(element) {
            var temp = element.getElementsByTagName('img')[0].alt;
            temp = "<a href='"+temp+"' style='font-size:50px;text-decoration: none;'>Click To Edit </a>";
            return temp;
        }
    });
    // baguetteBox.run('.tz-gallery'
    //     , {
    // captions: function(element) {
    //     return element.getElementsByTagName('img')[0].alt;
    // }
    // );
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
    function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
    }
</script>