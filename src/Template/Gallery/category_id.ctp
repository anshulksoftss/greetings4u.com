
<section id="about">
  <div class="container">

    <header class="section-header" style="padding-top:50px">
      <h3><?= $data->name ?></h3>
      <p><?= $data->description ?></p>
    </header>
    <div class="row">

    </div>
  </div>
</section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <!-- <h3>Services</h3> -->
          <!-- <p>ERP is business process management software that allows an organization to use a system of integrated applications.</p> -->
        </header>
        <div class="row">
            <?php foreach ($data->galery_img as $key => $value) { ?>
            <div class="col-md-6">
                <a href="<?= asset.'gallery/studio?imgNm='.$value->name.'&imgId='.$value->id ?>">
                    <figure class="figure">
                        <img src="<?= asset ?>img-gallery/<?= $value->name ?>" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure." style="width: 100%; max-height: 50vh">
                        <figcaption class="figure-caption rounded" id="img1" style="color:black;font-size:30px" ><?= $value->description ?></figcaption>
                    </figure>
                </a>
            </div>
            <?php } ?>
        </div>
      
      </div>
    </section><!-- #services -->