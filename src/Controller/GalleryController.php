<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use GDText\Box;
use GDText\Color;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class GalleryController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    private $session;

    public function initialize()
    {
        parent::initialize();
        $this->session = $this->request->session();
        $this->viewBuilder()->setLayout('home-layout');

        $this->loadComponent('Auth',[
            'authError' => 'Please Login to Continue...',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email','password'=>'password']
                ]
            ],
            'storage' => 'Session',
            'loginAction' => [
                'controller' => 'admin',
                'action' => 'login'
            ]

        ]);

        $this->Auth->allow(['messagesById', 'index', 'categoryId', 'festival', 'categoryName', 'studio', 'generateImage', 'addUser', 'chkUser']);


        $this->set('authUser',$this->Auth->user());

    }

    public function index()
    {
        $this->viewBuilder()->setLayout('default');
        $this->loadModel('MainCategory');
        $this->loadModel('DailyMessages');
        $this->loadModel('GalaryImg');
        
        $title = "Home";

        $data = $this->MainCategory->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1])
                                ->toArray();

        foreach ($data as $key => $value) {
            $img = $this->GalaryImg->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1, 'category'=>$value->id])
                                ->limit('5')
                                ->toArray();
            $value->galery_img = $img;

            $message = $this->DailyMessages->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1, 'main_category_id'=>$value->id])
                                // ->contain('GalaryImg')
                                ->toArray();
            $value->daily_msg = $message;
        }

        // pr($data); die;

        $this->set(compact(['title', 'data']));
    }

    public function messagesById($id='happy')
    {
        $this->loadModel('MainCategory');
        $this->loadModel('DailyMessages');
        $title = "Home";
        $name = '%'.$id.'%';
        $data = $this->MainCategory->findById($id)
                                ->where(['status'=>1])
                                ->first();
        
        $msg = $this->DailyMessages->find('all')
                                ->where(['status'=>'1', 'title like'=>$name])
                                ->limit('5')
                                ->toArray();
        // $data->galery_img = $msg;
        $data = $msg;
        $this->viewBuilder()->setLayout('ajax');

        $this->set(compact(['title', 'data']));
    }


    public function categoryId($id=null)
    {
        $this->loadModel('MainCategory');
        $this->loadModel('GalaryImg');
        $title = "Home";

        $data = $this->MainCategory->findById($id)
                                ->where(['status'=>1])
                                ->first();
        
            $img = $this->GalaryImg->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1, 'category'=>$data->id])
                                ->limit('5')
                                ->toArray();
            $data->galery_img = $img;

        $this->set(compact(['title', 'data']));
    }


    public function festival($id='null')
    {
        $this->loadModel('MainCategory');
        $this->loadModel('GalaryImg');
        $title = "Home";
        $name = '%'.$id.'%';

        $data = $this->MainCategory->find('all')
                                ->where(['status'=>'1'])
                                ->first();

        $img = $this->GalaryImg->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1, 'icon_name '=>$id])
                                ->limit('5')
                                ->toArray();
        $data->galery_img = $img;
        $data->name = 'Images';
        $data->description = $id;

        $this->set(compact(['title', 'data']));
    }

    public function categoryName($id)
    {
        $this->loadModel('MainCategory');
        $this->loadModel('GalaryImg');
        $title = "Home";
        $name = '%'.$id.'%';
        $data = $this->MainCategory->find('all')
                                ->where(['status'=>'1', 'name like'=>$name])
                                ->first();
        $img = $this->GalaryImg->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1, 'category'=>$data->id])
                                ->limit('5')
                                ->toArray();
        $data->galery_img = $img;

        $this->set(compact(['title', 'data']));
    }

    public function studio()
    {
        $title = "Studio";
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if($this->request->getQuery('imgNm') || $this->session->check('imgNm')){
            $this->session->write('imgNm', $this->request->getQuery('imgNm'));
            $this->session->write('imgId', $this->request->getQuery('imgId'));
            $img = $this->session->read('imgNm');
            $imgId = $this->session->read('imgId');
            $this->loadModel('GalaryImg');
            $data = $this->GalaryImg->findById($imgId)
                        ->contain('GalaryImgDetails')
                        ->first(); 
        }
        else{
            $this->Flash->error('Please Select Image.');
            return $this->redirect(['controller'=>'Gallery','action'=>'index']);
        } 
        
        $this->set(compact(['title', 'img', 'data', 'user']));
    }

    public function generateImage(){
        $title = "Studio";
        $footer = "Powered By ksoftss.com";

        if($this->session->check('imgNm')){

            $title = $this->request->getData('title');
            $fontsize = $this->request->getData('fontsize');
            $height = $this->request->getData('font_height');
            $width = $this->request->getData('font_width');
            $color = $this->request->getData('font_color');
            $fontstyle = $this->request->getData('fontstyle');

            $msg_title = $this->request->getData('msgtitle');
            $msg_fontstyle = $this->request->getData('msg_fontstyle');
            $msg_fontsize = $this->request->getData('msg_fontsize');
            $msg_height = $this->request->getData('msg_height');
            $msg_width = $this->request->getData('msg_width');

            $img_name = $this->session->read('imgNm');

            list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
            // echo $fontstyle; die;
            // $width += ((strlen($title)*$fontsize)/2)-$fontsize; 
            // $height += $fontsize; 
            // echo $height.'height'.$width; die;

            $fonturl = __DIR__.DS.'Fonts'.DS.$fontstyle.'.ttf'; 
            $fonturl_msg = __DIR__.DS.'Fonts'.DS.$msg_fontstyle.'.ttf'; 
            $fonturl_footer = __DIR__.DS.'Fonts'.DS.'Font.ttf'; 
            $imageurl= WWW_ROOT.'img-gallery'.DS.$img_name;

            file_put_contents("./image.png", file_get_contents($imageurl));
            

            $sizes = getimagesize("./image.png"); 
            $height = round(($sizes[1]*$height)/100);
            $width = round(($sizes[0]*$width)/100);

            $msg_height = round(($sizes[1]*$msg_height)/100);
            $msg_width = round(($sizes[0]*$msg_width)/100);

            $im = imagecreatefromjpeg($imageurl); 
            $backgroundColor = imagecolorallocate($im, 0, 0, 0);
            imagefill($im, 0, 0, $backgroundColor);

            $box = new Box($im);
            $box->setFontFace($fonturl); // http://www.dafont.com/franchise.font
            $box->setFontColor(new Color($r, $g, $b));
            $box->setFontSize($fontsize);
            $box->setBox(0, 0, 2*$width, 2*$height);
            $box->setTextAlign('center', 'center'); 
            $box->draw($title);

            if (!empty($msg_title)) {
                $box = new Box($im);
                $box->setFontFace($fonturl_msg); // http://www.dafont.com/franchise.font
                $box->setFontColor(new Color($r, $g, $b));
                $box->setFontSize($msg_fontsize);
                $box->setBox(0, 0, 2*$msg_width, 2*$msg_height);
                $box->setTextAlign('center', 'center');
                $box->draw($msg_title);
            }

            $box = new Box($im);
            $box->setFontFace($fonturl_footer); // http://www.dafont.com/pacifico.font
            $box->setFontColor(new Color(0, 0, 0));
            // $box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
            $box->setFontSize(30);
            $box->setBox(0,0, $sizes[0]-0, $sizes[1]-0);
            // $box->setBox(20,20, $height, $vertical);
            $box->setTextAlign('center', 'bottom');
            $box->draw($footer);

            // header("Content-Disposition: attachment");
            header('Content-Disposition: attachment; filename="image.png"');
            header("Content-type: image/png");
            imagepng($im);
        }
        else
        {
            $this->Flash->error('Please Select Image.');
            return $this->redirect(['controller'=>'Gallery','action'=>'index']);        
            $this->set(compact(['title']));
        } 
    }

    function imagecreatefromfile( $filename ) {
        if (!file_exists($filename)) {
            throw new InvalidArgumentException('File "'.$filename.'" not found.');
        }
        switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
            case 'jpeg':
            case 'jpg':
                return imagecreatefromjpeg($filename);
            break;

            case 'png':
                return imagecreatefrompng($filename);
            break;

            case 'gif':
                return imagecreatefromgif($filename);
            break;

            default:
                throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
            break;
        }
    }

    public function addUser()
    {
        
        $request=json_decode($this->request->getQuery('data'),true);
        $this->loadModel('Users');
        $this->session->write('username', $request["username"]);
        $user = $this->Users->newEntity();
        $user = $this->Users->patchEntity($user, $request);
        if ($this->Users->save($user)) {
            echo json_encode('success'); die;
        }
        else{
            echo json_encode('failed'); die;
        }
    }

    public function chkUser(){

        // $this->loadModel('Users');
        $username = $this->session->read('username'); 
        if($username){
            $request['status'] = "success";
            echo json_encode($request); die;
        }

        else{
            echo json_encode("failed");  die;
        }

    }
}
