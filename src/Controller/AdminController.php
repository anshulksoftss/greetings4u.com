<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use GDText\Box;
use GDText\Color;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdminController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    private $session;

    public function initialize()
    {
        parent::initialize();
        $this->session = $this->request->session();
        $this->viewBuilder()->setLayout('admin-layout');
        // $this->loadComponent('Auth',[
        //     'authError' => 'Please Login to Continue...',
        //     'authenticate' => [
        //         'Form' => [
        //             'fields' => ['username' => 'email','password'=>'password']
        //         ]
        //     ],
        //     'storage' => 'Session',
        //     'loginAction' => [
        //         'controller' => 'admin',
        //         'action' => 'login'
        //     ]

        // ]);

        // $this->Auth->allow(['registration', 'index', 'gallery', 'manage']);


        // $this->set('authUser',$this->Auth->user());

    }

    public function index()
    {
        $this->set(compact(['title', 'images']));
    }

    public function gallery()
    {
        $this->loadModel('GalaryImg');
        $title = "Gallery";
        $images = $this->GalaryImg
                        ->find('all')
                        ->order(['id'=>'desc'])
                        ->where(['status'=>1])
                        ->toArray();

        // pr($images); die;
        $this->set(compact(['title', 'images']));
    }

    public function manage()
    {
        $this->loadModel('MainCategory');
        $this->loadModel('DailyMessages');
        $title = "Manage";
        $categories = $this->MainCategory
                        ->find('all')
                        ->order(['id'=>'desc'])
                        ->where(['status'=>1])
                        ->toArray();
        
        $messages = $this->DailyMessages
                        ->find('all')
                        ->order(['id'=>'desc'])
                        ->where(['status'=>1])
                        ->toArray();

        // pr($images); die;
        $this->set(compact(['title', 'categories', 'messages']));
    }

    public function addCategory()
    {
        $this->loadModel('GalaryImg');
        $title = "Gallery";
        $images = $this->GalaryImg
                        ->find('all')
                        ->order(['id'=>'desc'])
                        ->where(['status'=>1])
                        ->toArray();

        // pr($images); die;
        $this->set(compact(['title', 'images']));
    }

    public function login()
    {   
        $this->loadModel('Auth');
        $this->viewBuilder()->layout('admin-login-layout');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            //pr($user); die ;
            if ($user && $user['user_type']=='admin') {

                $this->Auth->setUser($user);
                $user_id = $this->Auth->user('id');
                $session = $this->request->session();
                $token_id = $session->id();

                $this->loadModel('LoggedInUsers');
                $loggedInUser = $this->LoggedInUsers->newEntity();
                $loggedInUser->user_id = $user_id;
                $loggedInUser->token_id = $token_id;
                $loggedInUser->token_value = $token_value = strtoupper(substr(uniqid(sha1(time())),0,11));
                $session->write('loggedInUser',$loggedInUser); 
                

               // $this->set(compact('user_id','token_id','token_value'));


                if($this->LoggedInUsers->deleteAll(array('LoggedInUsers.user_id'=>$user_id)))
                {
                    if($this->LoggedInUsers->save($loggedInUser)){
                       return $this->redirect(['Controller'=>'Admin','action'=>'index']);
                    }
                }else{
                    if($this->LoggedInUsers->save($loggedInUser)){
                       return $this->redirect(['Controller'=>'Admin','action'=>'index']);
                    }
                }

                //return $this->redirect(['Controller'=>'Admin','action'=>'index']);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout(){

        //return $this->redirect($this->Auth->logout());
        $this->loadModel('LoggedInUsers');
        $user_id = $this->Auth->user('id');
        if($this->LoggedInUsers->deleteAll(array('LoggedInUsers.user_id'=>$user_id)))
        {
            return $this->redirect($this->Auth->logout());
        }
        $this->Auth->logout();
        return $this->redirect(['controller' => 'Admin']);

    }

    public function registration(){
        $this->loadModel('Users');
        $this->viewBuilder()->layout('admin-login-layout');
        $data=$this->Users->newEntity();
        if($this->request->is('post')){
            $form_data = $this->request->Data();
            $admin_pass = $form_data['password'];
            
            $admin=$this->Users->patchEntity($admin,$form_data);
            $created_by = $this->Auth->user('id');
            $admin->user_type = 'admin';
            $admin->created_by = $created_by;
            if($this->Users->save($admin)){
                $this->Audit->registerAdmin($admin);
                $data['to'] = $admin->email;
                    $var['admin_name'] = $admin->first_name."".$admin->last_name;
                    $var['admin_email'] = $admin->email;
                    $var['admin_pass'] = $admin_pass;
                $data['viewVars'] = $var;

                $adminMails = new AdminMails();
                $adminMails->welcomeAccountCreation($data);

                $this->Flash->success(__('The Admin has been saved.'));
                $this->redirect(['action'=>'updateAdminList']);
            }
        }
        $this->set(compact('data'));
    }

}
