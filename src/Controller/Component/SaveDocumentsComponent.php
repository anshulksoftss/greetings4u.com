<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;

class SaveDocumentsComponent extends Component
{ 
	public function saveDocs($data, $imgFolder, $fileName)
    {
        // print_r(json_encode($data['name']));
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,5));
        $unic_no = $today . $rand;

        $filename = $data['name'];
        $file_tmp_name = $data['tmp_name'];
        // $dir = WWW_ROOT.'img'.DS.'vehicle-docs-img/';
        $dir = WWW_ROOT.$imgFolder;
        $allowed = array('png','jpg', 'jpeg', 'bmp');
        $newfilename = $unic_no."-".str_replace(' ', '-', $filename); //$filename;
        $doc_path = $dir.$newfilename;

        if (!is_null($fileName)) {
            $newfilename = $fileName;
        }

        if (!in_array( substr( strrchr($filename, '.'),1),$allowed)) {
                //throw new Exception("Error Processing Request", 1); 
        }elseif (is_uploaded_file($file_tmp_name)) {
            if(move_uploaded_file($file_tmp_name, $doc_path))
            {    
                $newdata = (object) array('filename' => $newfilename, 'filepath' => $imgFolder.$newfilename);
                return $newdata;
            }
        }
    }


    public function filterSelectedMealMenu($data)
	{
        // pr($data['meal_menu_details']);
		foreach ($data['meal_menu_details'] as $key => $value) {
            if($value['item_id']>0){
                $meal_menu_details[$key]['item_id'] = $value['item_id'];
                $meal_menu_details[$key]['item_qty'] = $value['item_qty'];
            }
        }
        $data['meal_menu_details'] = $meal_menu_details;
        return $data;
	}
}