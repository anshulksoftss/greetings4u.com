<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use GDText\Box;
use GDText\Color;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ManageController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    private $session;

    public function initialize()
    {
        parent::initialize();
        $this->session = $this->request->session();
        $this->viewBuilder()->setLayout('admin-layout');

    }

    public function index()
    {
        $this->loadModel('GalaryImg');
        $title = "Gallery";
        $images = $this->GalaryImg->find('all')
                                ->order(['id'=>'desc'])
                                ->where(['status'=>1])
                                ->toArray();

        // pr($images); die;
        $this->set(compact(['title', 'images']));
    }
    
//Category
    public function addCategory(){
        $title = 'Add Category';
        $this->loadModel('MainCategory');

        $category = $this->MainCategory->newEntity(); 
        if($this->request->is(['post', 'patch'])){ 
            $this->MainCategory->patchEntity($category,$this->request->getData());
            $category->created_by = 1;
            $category->updated_by = 1;
            $category->status = 1;
            $category->user_ip = $this->request->clientIp();
            // checking category patching details and updating that assigned status
            if($this->MainCategory->save($category)){
                $this->Flash->success(__('Successfully Added'));
            }
            else{
                $this->Flash->error(__('Failed!')); 
            }
        }
        
        $this->set(compact(['title','category']));
    }

    public function editCategory($id)
    {
        $title = 'Edit Category';
        $this->loadModel('MainCategory');

        $category = $this->MainCategory->get($id);
        if($this->request->is(['post', 'patch'])){ 
            $this->MainCategory->patchEntity($category,$this->request->getData());
            $category->created_by = 1;
            $category->updated_by = 1;
            $category->status = 1;
            $category->user_ip = $this->request->clientIp();
            // checking category patching details and updating that assigned status
            if($this->MainCategory->save($category)){
                $this->Flash->success(__('Successfully Added'));
            }else{
                $this->Flash->error(__('Failed!')); 
            }
        } 
        $this->set(compact(['title','category']));
    }

    public function deleteCategory($id = null)
    {
        $this->loadModel('MainCategory');
        $mealMenus = $this->MainCategory->get($id);
        if ($this->MainCategory->Delete($mealMenus)) {
            $this->Flash->success(__('Delete Succesfully!'));
        } else {
            $this->Flash->error(__('Failed to Deleted!'));
        }

        return $this->redirect(['controller' => 'admin' ,'action' => 'manage']); 
    }

//Messages
    public function addMessage(){
        $title = 'Add Message';
        $this->loadModel('DailyMessages');
        $this->loadModel('MainCategory');

        $data = $this->DailyMessages->newEntity(); 
        $category = $this->MainCategory->find('all')->toArray(); 
        if($this->request->is(['post', 'patch'])){ 
            $this->DailyMessages->patchEntity($data,$this->request->getData());
            $data->created_by = 1;
            $data->updated_by = 1;
            $data->status = 1;
            $data->user_ip = $this->request->clientIp();
            // checking data patching details and updating that assigned status
            if($this->DailyMessages->save($data)){
                $this->Flash->success(__('Successfully Added'));
            }else{ pr($data->errors()); die;
                $this->Flash->error(__('Failed!')); 
            }
        }

        $this->set(compact(['title','data', 'category']));
    }

    public function editMessage($id)
    {
        $title = 'Edit Message';
        $this->loadModel('DailyMessages');

        $data = $this->DailyMessages->get($id);
        if($this->request->is(['post', 'patch'])){ 
            $this->DailyMessages->patchEntity($data,$this->request->getData());
            $data->created_by = 1;
            $data->updated_by = 1;
            $data->status = 1;
            $data->user_ip = $this->request->clientIp();
            // checking data patching details and updating that assigned status
            if($this->DailyMessages->save($data)){
                $this->Flash->success(__('Successfully Added'));
            }else{
                $this->Flash->error(__('Failed!')); 
            }
        } 
        $this->set(compact(['title','data']));
    }

    public function deleteMessage($id = null)
    {
        $this->loadModel('DailyMessages');
        $data = $this->DailyMessages->get($id);
        if ($this->DailyMessages->Delete($data)) {
            $this->Flash->success(__('Deleteded Succesfully!'));
        } else {
            $this->Flash->error(__('Failed to Deleted!'));
        }

        return $this->redirect(['controller' => 'admin' ,'action' => 'manage']); 
    }

//Image
    public function addImage(){
        $title = 'Add Image';
        $this->loadModel('GalaryImg');
        $this->loadModel('MainCategory');
        $this->loadComponent('SaveDocuments');

        $data = $this->GalaryImg->newEntity(); 
        $category = $this->MainCategory->find('all')->select(['id','name'])->toArray(); 
        if($this->request->is(['post', 'patch'])){ 
            $imgFolder = 'img-gallery/';
            $imgFolderThumbs = 'img-gallery/thumbs/';
            $image = $this->request->getData('uploaded_image');
            $image_thumb = $this->request->getData('upload_thumb_image');
            if($image){
                $data=$this->GalaryImg->patchEntity($data,$this->request->getData());
                $item_image = $this->SaveDocuments->saveDocs($image,$imgFolder, null);
                $item_image = $this->SaveDocuments->saveDocs($image_thumb,$imgFolderThumbs, $item_image->filename);
                $data->name = $item_image->filename;
                $data->created_by = 1;
                $data->updated_by = 1;
                $data->status = 1;
                $data->user_ip = $this->request->clientIp();
            }

            // checking data patching details and updating that assigned status
            if($this->GalaryImg->save($data)){
                $this->Flash->success(__('Image Added Successfully! Please Add Details'));
                return $this->redirect(['controller' => 'manage' ,'action' => 'addImageDtls', $data->id]); 
            }else{ pr($data->errors()); die;
                $this->Flash->error(__('Failed!')); 
            }
        }

        $this->set(compact(['title','data', 'category']));
    }

    public function deleteImage($id = null)
    {
        $this->loadModel('GalaryImg');
        $data = $this->GalaryImg->get($id);
        if ($this->GalaryImg->Delete($data)) {
            $this->Flash->success(__('Deleted Succesfully!'));
        } else {
            $this->Flash->error(__('Failed to Deleted!'));
        }

        return $this->redirect(['controller' => 'admin' ,'action' => 'gallery']); 
    }

//Image Details
    public function addImageDtls($galary_img_id = null){
        $title = 'Add Image Details';
        $this->loadModel('GalaryImgDetails');
        $this->loadModel('GalaryImg');
        $img_data = $this->GalaryImg->get($galary_img_id);
        $data = $this->GalaryImgDetails->newEntity(); 
        $data->galary_img_id = $galary_img_id;
        if($this->request->is(['post', 'patch'])){ 
            $this->GalaryImgDetails->patchEntity($data,$this->request->getData());
            $data->created_by = 1;
            $data->updated_by = 1;
            $data->status = 1;
            $data->user_ip = $this->request->clientIp();
            // pr($this->request->getData());
            // pr($data); die;
            // checking data patching details and updating that assigned status
            
            if($this->GalaryImgDetails->save($data)){ 
                $this->Flash->success(__('Successfully Added')); 
                return $this->redirect(['controller' => 'admin' ,'action' => 'gallery']); 
            }
            else{
                pr($data->errors()); die;
                $this->Flash->error(__('Failed!')); 
            }

        }
        $data->galary_img_id = $galary_img_id;
        $this->set(compact(['title','data', 'category', 'img_data']));
    }

    public function deleteImageDtls($id = null)
    {
        $this->loadModel('GalaryImgDetails');
        $data = $this->GalaryImgDetails->get($id);
        if ($this->GalaryImgDetails->Delete($data)) {
            $this->Flash->success(__('Deleteded Succesfully!'));
        } else {
            $this->Flash->error(__('Failed to Deleted!'));
        }

        return $this->redirect(['controller' => 'admin' ,'action' => 'gallery']); 
    }


}
