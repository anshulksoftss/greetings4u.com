<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GalaryImgTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GalaryImgTable Test Case
 */
class GalaryImgTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GalaryImgTable
     */
    public $GalaryImg;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.galary_img'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GalaryImg') ? [] : ['className' => GalaryImgTable::class];
        $this->GalaryImg = TableRegistry::getTableLocator()->get('GalaryImg', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GalaryImg);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
