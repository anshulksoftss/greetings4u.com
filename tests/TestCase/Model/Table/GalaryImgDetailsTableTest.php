<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GalaryImgDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GalaryImgDetailsTable Test Case
 */
class GalaryImgDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GalaryImgDetailsTable
     */
    public $GalaryImgDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.galary_img_details',
        'app.galary_imgs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GalaryImgDetails') ? [] : ['className' => GalaryImgDetailsTable::class];
        $this->GalaryImgDetails = TableRegistry::getTableLocator()->get('GalaryImgDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GalaryImgDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
