<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MainCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MainCategoryTable Test Case
 */
class MainCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MainCategoryTable
     */
    public $MainCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.main_category'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MainCategory') ? [] : ['className' => MainCategoryTable::class];
        $this->MainCategory = TableRegistry::getTableLocator()->get('MainCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MainCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
